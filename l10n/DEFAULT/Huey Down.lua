-- R3DL1N3/Huey Down.lua
--                    _______             ______   _______           _
-- |\     /||\     /|(  ____ \|\     /|  (  __  \ (  ___  )|\     /|( (    /|
-- | )   ( || )   ( || (    \/( \   / )  | (  \  )| (   ) || )   ( ||  \  ( |
-- | (___) || |   | || (__     \ (_) /   | |   ) || |   | || | _ | ||   \ | |
-- |  ___  || |   | ||  __)     \   /    | |   | || |   | || |( )| || (\ \) |
-- | (   ) || |   | || (         ) (     | |   ) || |   | || || || || | \   |
-- | )   ( || (___) || (____/\   | |     | (__/  )| (___) || () () || )  \  |
-- |/     \|(_______)(_______/   \_/     (______/ (_______)(_______)|/    )_)
--
-- Copyright © 2017, R3DL1N3, United Kingdom
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the “Software”), to
-- deal in the Software without restriction, including without limitation the
-- rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
-- sell copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
--   The above copyright notice and this permission notice shall be included in
--   all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED “AS IS,” WITHOUT WARRANTY OF ANY KIND, EITHER
-- EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
-- OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
-- ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- “Only the dead have seen the end of war.” Plato.
--
-- Garrison: “A long–anticipated meeting of Aidid’s senior cabinet may
-- take place today at 1500 hours. I say ‘may’ because we all know by now
-- with the intel we get on the street, nothing is certain. This is actual intel
-- confirmed by three sources. Two tier–one personalities may be present: Omar
-- Salad, Aidid’s top political advisor, and Abdi Hassan Awale, Interior
-- Minister. These are the guys we’re after. Today we go. Same mission
-- template as before. 1545, Assault Force Delta, will infiltrate the target
-- building and seize all suspects within. Security Force Rangers: 4 Ranger
-- chalks, under command of Captain Steele, will rope in at 1546 and hold a
-- four–corner perimeter around the target building. No one gets in or out.
-- Extraction force: Lt. Colonel McKnight’s Humvee column will drive in the
-- city at 1547 on Hawlwadig Road and hold just short of the Olympic Hotel, and
-- wait for the green light. Now once Delta gives the word, McKnight’s column
-- will move to the target building and load the prisoners onto flatbed trucks.
-- Immediately after the prisoners are loaded, the four Ranger chalks will
-- collapse back to the target building, load up on Humvees, and the entire
-- ground force will exfil the three miles back to the base. Mission time, from
-- incursion to extraction, no longer than 30 minutes…”
--
-- McKnight: “No Spectre gunships, daylight instead of night, late afternoon
-- when they’re all fucked up on Khat, only part of the city Aidid can mount a
-- serious counter–attack on short notice… What’s not to like?”
-- Harell: “Life’s imperfect.”
-- McKnight: “Yeah, for you two, circling above it at five hundred feet,
-- it’s imperfect. Down in the street, it’s unforgiving.”
--
-- Atto: “I see not catching Aidid is becoming routine.”
-- Garrison: “We’re not leaving Somalia until we find him. And we will find
-- him.”
-- Atto: “Don’t make the mistake of thinking because I grew up without
-- running water I am simple General. I do know something about history. See
-- all this? It’s simply shaping tomorrow. A tomorrow without a lot of
-- Arkansas white–boys’ ideas in it.”
-- Garrison: “Well, I wouldn’t know about that. I’m from Texas.”
-- Atto: “Mr. Garrison, I think you should not have come here. This is civil
-- war. This is our war, not yours.”
-- Garrison: “Three hundred thousand dead and counting. That’s not a war,
-- Mr. Atto. That’s genocide.”
--
-- 1. Pay off our Somali spy, Abdi. Deliver Khat, dollars or shillings from the
--    airport to his yard, north–east end of Mogadishu. Red smoke will mark
--    the site. Rope length is 50 feet. Take care to drop a crate in his back
--    yard only. You can damage it, but not destroy it.
-- 2. Abdi will drive his car to where Aidid’s top political advisors are
--    secretly meeting today: Omar Salad, Aidid’s top political advisor, and
--    Abdi Hassan Awale, Interior Minister. Capture or kill them. If you follow
--    Abdi’s car, take care not to engage it.
-- 3. At that moment, Lt. Colonel Matthews will give the assault code word,
--    “Irene.” McNight’s Humvee column Uniform 6 will converge at the
--    location. Super 6 flight will drop Delta operators and Rangers to capture
--    or kill the targets. Embark them at the airport. Star 4 flight will
--    support with 20mm cannon and 68mm rockets.
-- 4. All units return safely to Mogadishu Airport.
--
-- Tactics
-- =======
-- * You’ll need at least three good chopper pilots skilled with Hueys and
--   Gazelles.
-- * Embark and disembark chalks and fireteams simply by landing and opening the
--   main door. If your chopper is already transporting troops, they disembark
--   around you. Otherwise, troops embark if they are within 100 metres of each
--   other and you are in 100 metres of them. Hueys transport eight–man
--   highly–skilled Ranger chalks. Gazelles transport excellently–skilled
--   four–man Delta operator fireteams. Deploy them wisely.
-- * Protect McKnight’s convoy, Uniform 6. Rescue survivors if helicopters go
--   down. Look out for flare pops, one every 10 seconds while survivors remain
--   alive. You get 10 points added to your score for every survivor returned to
--   the airport.
-- * The Somalis swarm. They carry mobile phones and hate “Arkansas
--   white–boys.” Tire–fires tell you where they’re heading.
--
--------------------------------------------------------------------------------

-- "Short cuts make long delays," J.R.R. Tolkien. But not always, Pippin.
local BLUE = coalition.side.BLUE
local RED = coalition.side.RED
local USA = country.id.USA
local INSURGENTS = country.id.INSURGENTS
local GROUND = Group.Category.GROUND

-- As a matter of course, send Killed-In-Action messages to both sides, but only
-- if the initiating dead unit has a name and the name is not blank, or a number.
world.addEventFunction(function(event)
  if not event.initiator then return end
  if event.id == world.event.S_EVENT_DEAD then
    local name = event.initiator:getName()
    if name and type(name) == 'string' and name ~= '' then
      trigger.action.outText('KIA ' .. name, 3)
    end
  end
end)

function Unit:addScore(key, score)
  self:addPlayerScore(key, 1)
  self:addPlayerScore('Score', score)
end

world.addEventFunction(function(event)
  if event.id == world.event.S_EVENT_PLAYER_SCORED then
    UserFlag['77'] = (UserFlag['77'] or 0) + 1
  end
end)

-- Crashes deduct 50 from your score, ejects 10 and leaving the slot 25. Hits
-- add 1 to score, unless friendly fire; such deduct 10 per hit. Be careful.
-- Kills get hit points but such multipled by 10 and deducted for friendly kills.
world.addEventFunction(function(event)
  if not event.initiator then return end
  if event.id == world.event.S_EVENT_CRASH then
    event.initiator:addScore('crashes', -50)
  elseif event.id == world.event.S_EVENT_EJECTION then
    event.initiator:addScore('ejects', -10)
  elseif event.id == world.event.S_EVENT_PLAYER_LEAVE_UNIT then
    event.initiator:addScore('leaves', -25)
  elseif event.id == world.event.S_EVENT_HIT then
    local score
    if event.initiator:isHostileWith(event.target) then
      score = event.initiator:disembarkedBy() and 3 or 1
    elseif event.initiator:isFriendlyWith(event.target) then
      score = -10
    else
      score = 0
    end
    event.initiator:addScore('hits', score)
    if event.target:getLife() < 1 then
      local life = event.target.getLife0 and event.target:getLife0() or 1
      event.initiator:addScore('kills', life * score)
    end
    -- Adds 50 points for every hit on the Aidid group.
    if event.initiator:getGroup():getName() == 'Aidid' then
      event.initiator:addScore('Aidid', 50)
    end
  end
end)

--------------------------------------------------------------------------------
--                                                                        Ranger
--------------------------------------------------------------------------------

Ranger = {
  names = Names('B/3/75 Ranger Chalk #', 'B/3/75 Ranger #'),
}

-- Spawns a Ranger chalk in the zone when empty.
function Ranger.spawnInZone(zone)
  if #Unit.allInZone(zone, BLUE) == 0 then
    local units = Units()
    units:addType('Soldier M4', 7)
    units:addType('Soldier M249', 1)
    units:setHighSkill()
    units:setRandomTransportable(true)
    units:formSquareInZones(zone, Zone('Ranger'))
    Ranger.names:applyTo(units)
    units:spawn(USA, GROUND)
  end
end

-- Answers true if the group is a Ranger group, based on its name.
function Group:isRanger()
  return Ranger.names:includesGroup(self)
end

--------------------------------------------------------------------------------
--                                                                         Delta
--------------------------------------------------------------------------------

Delta = {
  names = Names('C/1 Delta Fireteam #', 'C/1 Delta #'),
}

-- Spawns a Delta fireteam in the zone when empty.
function Delta.spawnInZone(zone)
  if #Unit.allInZone(zone, BLUE) == 0 then
    local units = Units()
    units:addType('Soldier M4 GRG', 4)
    units:setExcellentSkill()
    units:setRandomTransportable(true)
    units:formSquareInZones(zone, Zone('Delta'))
    Delta.names:applyTo(units)
    units:spawn(USA, GROUND)
  end
end

-- Answers true if the group is a Delta group, based on its name.
function Group:isDelta()
  return Delta.names:includesGroup(self)
end

--------------------------------------------------------------------------------
--                                                                        Chalks
--------------------------------------------------------------------------------

-- If a Huey lands near the Ranger zone, pick up a chalk. If a Gazelle lands
-- near the Delta zone, pick up a fireteam. If a chopper goes down, the troops
-- go down with it. They become survivors with reduced skill and condition.

Star = {}

function Group:isWaitingForRescue()
  return self:isSurvivor() and not self:inZone(Zone('Mogadishu Airport'))
end

-- Embark groups even if tasked. Star can embark survivors even those from a
-- Super crash, but only if the size of the group is less than or equal to four.
function Star.embarkOrDisembark(unit)
  if not unit then return end
  if not unit:isLanded() then return end
  local groups = table.fromiter(Group.filtered(BLUE, GROUND, function(group)
    local size = group:getSize()
    return size > 0 and size <= 4 and (group:isDelta() or group:isWaitingForRescue())
  end))
  unit:embarkOrDisembark(groups, 100, 100)
end

Super = {}

function Super.embarkOrDisembark(unit)
  if not unit then return end
  if not unit:isLanded() then return end
  local groups = table.fromiter(Group.filtered(BLUE, GROUND, function(group)
    return group:getSize() > 0 and (group:isRanger() or group:isWaitingForRescue())
  end))
  unit:embarkOrDisembark(groups, 100, 100)
end

-- Looks for changes to player unit's in-air status. Helicopters land when they
-- change from in-air to not in-air. On landing, either embark or disembark a
-- chalk.
world.addEventFunction(function(event)
  if event.id == world.event.S_EVENT_DISEMBARKING then
    if not event.initiator:inZone(Zone('Mogadishu Airport')) then return end
    if event.units:isSurvivor() then
      local score = 0
      for _, unit in ipairs(event.units:all()) do
        score = score + 10 * math.floor(unit.life or 1)
      end
      if score > 0 then
        event.initiator:addScore('rescues', score)
      end
    end
  elseif event.id == world.event.S_EVENT_EMBARKED then
    local text = event.initiator:getName() .. ' embarked ' .. event.units.name
    trigger.action.outTextForCoalition(BLUE, text, 3)
  elseif event.id == world.event.S_EVENT_DISEMBARKED then
    local text = event.initiator:getName() .. ' disembarked ' .. event.units.name
    trigger.action.outTextForCoalition(BLUE, text, 3)
  end
end)

--------------------------------------------------------------------------------
--                                                                         Crash
--------------------------------------------------------------------------------

-- Spawn a crash site when a chopper crashes. One to three survivors emerge. The
-- survivors pop flares if they remain around the crash site, and remain alive.
-- Crash sites close when a chopper dusts off at the site for between 60 and 90
-- seconds, collecting bodies and destroying the remaining equipment.
--
-- Spawns a crash site at the point of the crash whenever there's a crash event,
-- even if the crash occurs inside Mogadishu airport. This is somewhat open to
-- abuse. Helicopters could crash at the airport and make survivors for rescue
-- even without leaving the Mogadishu safe space.
--
-- Triggers an explosion at the crash site. Cleans it. Be careful not to make
-- the explosion too powerful. It might kill the escaping survivors, e.g. 100
-- will kill them as they run away.
world.addEventFunction(function(event)
  if event.id == world.event.S_EVENT_CRASH_SPAWNED then
    UserFlag['748'] = true
    local typeName = ({
      ['UH-1H'] = 'Huey',
      ['SA342L'] = 'Gazelle',
    })[event.crash.typeName] or 'chopper'
    local text = 'Harell: “We got a ' .. typeName .. ' down, we got a ' .. typeName .. ' down.”'
    trigger.action.outTextForCoalition(event.crash.side, text, 30)
    local zone = Zone('Mogadishu')
    if math.distancexz(event.crash.zone.point, zone.point) < zone.radius then
      local text = 'Matthews: “' .. event.crash.name .. ' is down. We got a bird down in the city.”'
      trigger.action.outTextForCoalition(event.crash.side, text, 30)
    end
  elseif event.id == world.event.S_EVENT_CRASH_CLEANED then
    trigger.action.explosion(event.crash.zone.point, 1)
  end
end)

--------------------------------------------------------------------------------
--                                                                       Militia
--------------------------------------------------------------------------------

Militia = {
  names = Names('Militiamen #', 'Militiaman #'),
}

-- Spawns a militia fireteam in the zone when empty.
function Militia.spawnInZone(zone, force)
  if force == true or #Unit.allInZone(zone, RED) == 0 then
    local units = Units()
    units:addType('Soldier AK', 3)
    units:addType('Soldier RPG', 1)
    units:setGoodSkill()
    units:setRandomTransportable(false)
    units:randomizeXYInZone(zone)
    units:randomizeHeading()
    Militia.names:applyTo(units)
    units:spawn(INSURGENTS, GROUND)
  end
end

--------------------------------------------------------------------------------
--                                                                        Somali
--------------------------------------------------------------------------------

Somali = {
  names = Names('Somalis'),
}

function Group:isSomali()
  return Somali.names:includesGroup(self)
end

-- Returns true if the unit is a Somali, a dynamically-generated Somali. Uses
-- the unit name matching the unit name prefix to make that determination. Do
-- not confuse this by naming static units with the same name prefix.
function Unit:isSomali()
  return Somali.names:includesUnit(self)
end

-- Spawn Somali insurgents in the given zone if the zone contains blues,
-- excluding blue helicopters. Limits the number of Somali insurgents based on
-- the zone radius.
function Somali.spawnInZone(zone)
  local blues = Unit.allInZone(zone, BLUE)
  local reds = Unit.allInZone(zone, RED, GROUND)
  if #blues > 0 and #reds < zone.radius / 20 then
    local units = Units()
    units:addType('Infantry AK Ins', 8)
    -- The following makes Iglas appear amongst the general Mogadishu
    -- population. It makes sure that Hueys do go down.
    -- if #Crashes == 0 then
    --   units:addType('Igla manpad INS')
    -- end
    units:setAverageSkill()
    units:setRandomTransportable(false)
    -- Pick a random zone within the zone, so that the units within the zone
    -- spawn in roughly the same area.
    units:randomizeXYInZone({point = math.randomxzinzone(zone), radius = 300})
    units:randomizeHeading()
    Somali.names:applyTo(units)
    units:spawn(INSURGENTS, GROUND)
  end
end

-- Sets up Somali counter attacks. Untasked Somali ground units turn towards the
-- nearest target within their zone. The Somalis are drugged up on Khat. Only
-- applies to Somalis, not to militiamen.
--
-- Does nothing if there are red players slotted. This allows the red side to
-- control the Somali population freely, as well as the militia.
function Somali.attackInZone(zone)
  if #coalition.getPlayers(RED) > 0 then return end
  local units = Unit.allInZone(zone, BLUE, GROUND)
  if #units == 0 then return end
  for group in Group.filtered(RED, GROUND, function(group)
    return group:isSomali() and not group:getController():hasTask()
  end) do
    group:setTurningToUnitsTask(units, AI.Task.VehicleFormation.ON_ROAD, 10)
  end
end

-- Removes Somalis in the given zone. Does nothing if the zone contains blue
-- ground units. Picks just one group at random for destruction. Invoke this
-- periodically in order to progressively retire the Somali population of
-- Mogadishu.
function Somali.destroyInZone(zone)
  local groups = table.fromiter(Group.filtered(RED, GROUND, function(group)
    return group:isSomali() and group:getSize() > 0
  end))
  if #groups == 0 then return end
  local units = Unit.allInZone(zone, BLUE, GROUND)
  if #units == 0 then
    groups[math.random(1, #groups)]:destroy()
  else
    table.sort(groups, function(lhs, rhs)
      return lhs:sortUnitsByDistance(units)[1]:distanceFromGroup(lhs) > rhs:sortUnitsByDistance(units)[1]:distanceFromGroup(rhs)
    end)
    groups[1]:destroy()
  end
end

Aidid = {}

-- Generates a random zone within the Olympic Hotel zone.
function Aidid.randomizeZone()
  if Aidid.zone then return end
  Aidid.zone = {
    point = math.randomxzinzone(Zone('Olympic Hotel')),
    radius = 100
  }
end

function Aidid.spawn()
  do
    local units = Units()
    units:add{type = 'Infantry AK Ins', name = 'Mohamed Hassan Awale'}
    units:add{type = 'Infantry AK Ins', name = 'Omar Salad Elmi'}
    units:setExcellentSkill()
    units:setRandomTransportable(false)
    units:randomizeXYInZone(Aidid.zone)
    units:randomizeHeading()
    units.name = 'Aidid'
    units:spawn(INSURGENTS, GROUND)
  end
  Militia.spawnInZone(Aidid.zone, true)
  Militia.spawnInZone(Aidid.zone, true)
  do
    local units = Units()
    units:addType('Igla manpad INS', 3)
    units:setGoodSkill()
    units:setRandomTransportable(false)
    units:randomizeXYInZone(Aidid.zone)
    units:randomizeHeading()
    Militia.names:applyTo(units)
    units:spawn(INSURGENTS, GROUND)
  end
end

Abdi = {}

-- Runs once when Abdi gets his stash. Be careful. Abdi is vulnerable and open
-- to misdirection. Do not shoot him. And if you play as red, do not misdirect
-- him. He needs to be red in order not to be attacked by Somalis, but he's
-- really a blue Somali spy.
function Abdi.go()
  if not Aidid.zone then return end
  local group = Group('Abdi')
  local mission = group:turningToPointMission(Aidid.zone.point, AI.Task.VehicleFormation.ON_ROAD)
  -- Abdi goes off like a shot, 30 mph. So fast that he often careens off the
  -- road. You can see the dust trail. 39 mph is his maximum speed.
  mission:waypoints()[1].speed = 13.4112
  mission:pushTaskTo(group:getController())
end

function Abdi.stop()
  if not Aidid.zone then return end
  if not Unit('Abdi'):inZone(Aidid.zone) then return end
  if UserFlag['14323'] == true then return end
  UserFlag['14323'] = true
  Group('Abdi'):getController():resetTask()
  Aidid.spawn()
  Group('Uniform 6'):turnToPoint(Aidid.zone.point, AI.Task.VehicleFormation.ON_ROAD, 8.9408)
  Group('Militia Technicals'):turnToPoint(Aidid.zone.point, AI.Task.VehicleFormation.ON_ROAD, 8.9408)
end
