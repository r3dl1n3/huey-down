# Change Log

## Version 0.10 (17 August 2017)

Minor fix.

- Guard again seeing empty groups before clean-up

## Version 0.9 (17 August 2017)

Updated for DCS version 2.1.1.8491, Update 2.

- Adjusted the spawning and swarming of Somalis. Version 2.1.x currently has
  a 'feature,' i.e. bug, which stalls the group AI controller. Groups can stop
  responding to tasking. Added workaround by cleaning up distant Somali mobs.
- Changed Delta fireteam to all have unit type Soldier M4 GRG. Looks more like
  Delta, with kneepads and small automatic rifles.

### Important VR Headset Note

Make sure that the visible range is High not Medium. DCS defaults to medium
when selecting VR. However, this reduces the level-of-detail setting for trees.
Trees will be there and you can collide with them, you just will not be able to
see them! Select VR defaults, then increase _Visib Range_ from Medium to High.

## Version 0.8 (17 May 2017)

- Abdi moved from red to blue

Huey AI gunners could engage Abdi, even though he's marked "invisible" to AI.
Corrected by making him Blue. This also makes it easier for Super-6 and Star-4
flights to follow him on the F10 map. Many thanks to the pilots of the 924th
Virtual Fighter Wing for their hard-core testing.

## Version 0.7 (11 May 2017)

- Fix chalk rotation: heading increases anti-clockwise
- Smaller mission file: sound files at 22kHz mono

## Version 0.6 (10 May 2017)

- Abdi is "invisible" to AI.
- Give Uniform 6 and the Technicals a speed of 20mph
- Give the Group Destroy All Empty trigger a name
- Never assume that there is an event initiator; even when logically there should be

## Version 0.5 (22 April 2017)

- Fixed points awarded for survivor rescue
- Briefing: note about crash survivors and green flares
- Trigger periodic mission sweeper, maintain high frame rate

## Version 0.4 (3 April 2017)

- Updated hit scoring. Hits by disembarked groups receive triple scores for the
  player that dropped them.
- Reports scores every 150 seconds, at least.

## Version 0.3 (2 April 2017)

- Rescue survivors
- Enable rescue scores
- No green smoke marker
- Add 50 points to score for each Aidid unit hit
- Smaller 100 metre Aidid zone
- Speed up Abdi down to 30 mph using roads
- Randomise the target locations
- Embarking-disembarking for all flights using door opening
- Choppers can embark tasked groups
- Convoy is Uniform 6
- Chevy 2 renamed to Chevy 6 (can’t have Romeo 6)
- Consistent naming; McKnight’s convoy is Uniform 6
- Added the Olympic Hotel zone
- Make Abdi immortal
- Trigger score reports
- Somalis more spread out
- Rangers and Delta operators form squares

## Version 0.2 (25 March 2017)

After testing with Kez.

- Reduced soundtrack volumes. Normalised by -10 dB.
- Huey down flare delay reduced to 10 seconds.
- Ranger chalk size increase to eight.
- Delta fireteam size increased to four, with two M249 operators.
- All helos start hot.
- Somalis spawn in groups closer together; size increased from 7 to 8 units.
- Somalis do not swarm with a red player slotted.
- Somali militiamen spawn at their encampment to the north.
- Chalks and fireteams embark and disembark automatically.
- Changed map view to fog-of-war.
- Added Humvees for airport defense.

Left to do: Abdi's car needs changing from red to neutral so that blue gunners
do not attack him. Them doing so stalls the game. Briefing needs just the
bottom line.

## Version 0.1 (25 March 2017)

Initial version.
